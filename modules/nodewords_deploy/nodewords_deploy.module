<?php

/**
 * Implementation of hook_deploy().
 * @param $id
 *   The id of the nodewords entry we're deploying.
 * @return
 *   The results of our xmlrpc call.
 */
function nodewords_deploy_deploy($id) {
  static $xmlrpcusererr;
  // Create type-to-table name mappings in which we can specify some important
  // information about these tables and how they need to be handled.
  $types = array(
    NODEWORDS_TYPE_PAGE => array(
      'table' => 'nodewords_custom',
      'key' => 'pid',
      // Path isn't really a key on this table but it should be so we're going to
      // treat it like one, specifying it as the 'lookup' field for this table.
      'lookup' => 'path',
      // Specify any fields whose value needs to be run through an alter 
      // function before deployment.
      'value_alter' => array(
        'field' => 'path',
        'function' => 'nw_deploy_replace_nid'
      )
    ),
    NODEWORDS_TYPE_NODE => array(
      // Specify any fields whose value needs to be run through an alter 
      // function before deployment.
      'table' => 'nodewords',
      'value_alter' => array(
        'field' => 'id',
        'function' => 'nw_deploy_replace_nid'
      )
    ),
    NODEWORDS_TYPE_TERM => array(
      // Specify any fields whose value needs to be run through an alter 
      // function before deployment.
      'table' => 'nodewords',
      'value_alter' => array(
        'field' => 'id',
        'function' => 'nw_deploy_replace_tid'
      )
    ),
  );
  $result = db_query("SELECT * FROM {nodewords} WHERE mtid = %d", $id);
  if ($row = db_fetch_object($result)) {
    $nw_obj = new stdClass();
    // hold an array of the tables we'll be deploying
    $nw_obj->tables = array();
    $nw_obj->tables['nodewords'] = &$row;
    if ($row->type > 1) {
      if (isset($types[$row->type])) {
        $table = $types[$row->type]['table'];
        if ($table != 'nodewords') {
          // This means the id field in the nodewords table holds a lookup key for
          // one of its own custom tables, so we'll need to deploy the relevant
          // row from that table too.
          $key = $types[$row->type]['key'];
          $res = db_query("SELECT * FROM {{$table}} WHERE {$key} = %d", $row->id);
          if ($r = db_fetch_object($res)) {
            $r->key = $key;
            $r->lookup = $types[$row->type]['lookup'];
            $table = $types[$row->type]['table'];
            // We may need to alter certain values, e.g. paths with nids in them,
            // before deploying.
            if (isset($types[$row->type]['value_alter'])) {
              $field = $types[$row->type]['value_alter']['field'];
              $value = $r->{$field};
              $alter_func = $types[$row->type]['value_alter']['function'];
              $new_value = $alter_func($value);
              if (!$new_value) {
                //something went wrong
                return xmlrpc_error($xmlrpcusererr + 1, t('Problem deploying nodewords - dependencies not yet deployed.'));
              }
              $r->{$field} = $new_value;
            }
            $nw_obj->tables[$table] = $r;
          }
        }
        else {
            if (isset($types[$row->type]['value_alter'])) {
              // Nodewords uses the id field in the nodewords table to store
              // EITHER a lookup key for one of its own custom tables OR
              // a nid or tid, depending on the nodewords TYPE. If our type is a
              // nid or tid we need to switch it out for its remote key before
              // deployment.
              $value = $row->id;
              $alter_func = $types[$row->type]['value_alter']['function'];
              $new_value = $alter_func($value);
              if (!$new_value) {
                //something went wrong
                return xmlrpc_error($xmlrpcusererr + 1, t('Problem deploying nodewords - dependencies not yet deployed.'));
              }
              $row->id = $new_value;
            }
        }

      }
    }
  }
  return deploy_send(array('nodewords.save'), array($nw_obj));
}

/**
 * Implementation of incdeploy_info().
 */
function nodewords_deploy_incdeploy_info() {
  return array(
    'nodewords' => array('add', 'edit'),
  );
}

/**
 * Implementation of hook_incremental_deploy_update().
 *
 */
function nodewords_deploy_incremental_deploy_update($pid, $ts, $blocked_pid, $postponed_pid) {
  // Add all items from the nodewords table that have changed
  // since last deploy, excluding blocked and postponed items
  
  $triggered = db_query("SELECT DISTINCT nda.mtid, nw.name FROM {nodewords_deploy_audit_log} nda INNER JOIN {nodewords} nw ON nda.mtid = nw.mtid WHERE event_timestamp > %d", $ts);
  $module = 'nodewords_deploy';

  while ($row = db_fetch_array($triggered)) {
    // if this item is not in the active, postponed or blocked plan, add it
    // to the active plan
    if (!deploy_item_is_in_plans(array($pid, $blocked_pid, $postponed_pid), $module, $row['mtid'])) {
      $parent_iid = 0;
      // If this is a metatag for a particular node, add the node to the plan as the
      // parent of this item.
      $result = db_query("SELECT type, id FROM {nodewords} WHERE mtid = %d", $row['mtid']);
      if ($nw = db_fetch_array($result)) {
        if ($nw['type'] == NODEWORDS_TYPE_NODE) {
          // The id then is actually a node id - grab that node
          $parent_iid = incremental_deploy_get_parent_item('node', $nw['id'], $pid);
        }
      }
      incremental_deploy_add_to_plan($pid, 'nodewords_deploy', 'Nodewords: '. $row['mtid'] .' ('. $row['name'] .')', $row['mtid'], 0, DEPLOY_NODE_GROUP_WEIGHT + 1, $parent_iid);
    }
  }
}

/**
 * Function for altering the path used in a custom nodeword config in case it
 * is a node path and needs its nid swapped out for the remote nid.
 */
function nw_deploy_replace_nid($nid) {
  if (is_numeric($nid)) {
    // We've been passed a straigh nid, swap it out.
    $node = node_load($nid);
    $remote_key = deploy_get_remote_key($node->uuid, 'node');
    if (!$remote_key || !isset($remote_key['nid'])) {
      return NULL;
    }
    return $remote_key['nid'];
  }
  else {
    // We've been passed a node path, replace the nid and swap it out.
    $path_ar = explode('/', $path);
    if ($path_ar[0] == 'node' && is_numeric($path_ar[1])) {
      $nid = $path_ar[1];
      $node = node_load($nid);
      $remote_key = deploy_get_remote_key($node->uuid, 'node');
      if (!$remote_key || !isset($remote_key['nid'])) {
        return NULL;
      }
      $path = 'node/'. $remote_key['nid'];
    }
    return $path;
  }
}

function nw_deploy_replace_tid($tid) {
  $remote_key = deploy_get_remote_key(deploy_uuid_get_term_uuid($tid), 'term_data');
  return $remote_key['tid'];
}

/**
* Implements hook_node_deploy
*
* Edit a node before it gets sent off. This fixes the problem of some of the
* form fields provided by nodewords module on node forms, where checkboxes
* result in 0-indexed array elements.
* @param object $node A reference to the node that will be deployed
* @return $node The edited version
*/
function nodewords_deploy_node_deploy(&$node) {
  // Fix robots checkbox
  if (isset($node->nodewords['robots']['value'][0])) {
    unset($node->nodewords['robots']['value'][0]);
  }

  return $node;
}