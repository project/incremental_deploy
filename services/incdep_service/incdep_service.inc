<?php
// $Id $

/**
 * @file
 *  Various services required for deployment functionality in Incremental
 *  Deploy module.
 */


/**
 * Return and array of all the enabled languages.
 *
 * @return array
 *   An array containing all enabled languages
 *
 */
function incdep_service_getlanguages() {
  return language_list();
}

/**
 * Save a language on the remote site
 */
function incdep_service_save_language($language) {
  require_once('includes/locale.inc');
  $language_list = language_list();
  if (!isset($language_list[$language['language']])) {
    locale_add_language($language['language'], $language['name'], $language['native'], $language['direction'], $language['domain'], $language['prefix']);
    // If we are using Incremental Deploy module here to push changes to a further
    // step in the chain, then we need to add this new language to the active
    // plan seeing as form submission, which usually adds it, is not happening here.
    if (module_exists('incremental_deploy')) {
      $pid = incremental_deploy_get_active_pid();
      deploy_add_to_plan($pid, 'incremental_deploy' , 'Add language: '. $language['language'], 'language_enable__data__'. $language['language'], 0, DEPLOY_SYSTEM_SETTINGS_GROUP_WEIGHT);
    }
  }

  // Confirm that the language is now in the language list (passing TRUE here
  // ensures the list is getting rebuilt and should include our new language).
  $language_list = language_list('language', TRUE);
  // If anything else should happen once a new language is added, do it now.
  module_invoke_all('incdeploy_update_language', $language['language'], 'add');
  return isset($language_list[$language['language']]) ? 1 : services_error('Language could not be enabled on remote');
}


/**
 * Delete a language on the remote site.
 */
function incdep_service_delete_language($language) {
  watchdog('inc_dep_service', print_r($language, TRUE));
  require_once('includes/locale.inc');
  $language_list = language_list();
  if (isset($language_list[$language['language']])) {
    if (language_default('language') != $language['language'] && $language['language'] != 'en') {
      $form_state = array(
        'values' => array('langcode' => $language['language']),
      );
      drupal_execute('locale_languages_delete_form', $form_state);
    }
    else {
      return services_error('You cannot delete the default language or the en language');
    }
  }
  // Confirm that the language is not in our newly rebuild language list.
  $language_list = language_list('language', TRUE);
  return !isset($language_list[$language['language']]) ? 1 : services_error('Language could not be deleted on remote');
}

/**
 * Enable a language on the remote site.
 */
function incdep_service_enable_language($language) {
  $language_list = language_list();
  $lang = isset($language_list[$language]) ? $language_list[$language] : NULL;
  if (!$lang) {
    return services_error('You cannot enable a language that doesn\'t exist on the remote site.');
  }
  if (!$lang->enabled) {
    $count = variable_get('language_count', 0);
    db_query("UPDATE {languages} SET enabled = 1 WHERE language = '%s'", $language);
    variable_set('language_count', ++$count);
  }
    
  // Confirm that the language has been enabled.
  $language_list = language_list('language', TRUE);
  return $language_list[$language]->enabled ? 1 : services_error('Language could not be enabled on remote');
}

/**
 * Disable a language on the remote site.
 */
function incdep_service_disable_language($language) {
  $language_list = language_list();
  $lang = isset($language_list[$language]) ? $language_list[$language] : NULL;
  if (!$lang) {
    return services_error('You cannot disable a language that doesn\'t exist on the remote site.', 400);
  }
  if ($lang->enabled) {
    $count = variable_get('language_count', 0);
    db_query("UPDATE {languages} SET enabled = 0 WHERE language = '%s'", $language);
    variable_set('language_count', --$count);
  }
    
  // Confirm that the language has been disabled.
  $language_list = language_list('language', TRUE);
  return ($language_list[$language]->enabled == 0) ? 1 : services_error('Language could not be disabled on remote');
}

/**
 * Saves a block setting.
 */
function incdep_service_save_block($block) {
  $block = (object) $block;
  
  db_query("REPLACE INTO {blocks} (bid, module, delta, theme, status, weight, region, custom, throttle, visibility, pages, title, cache) VALUES (%d, '%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)", $block->bid, $block->module, $block->delta, $block->theme, $block->status, $block->weight, $block->region, $block->custom, $block->throttle, $block->visibility, $block->pages, $block->title, $block->cache);
  if (isset($block->box)) {
    $box = (object) $block->box;
    watchdog('custom services', print_r($box, TRUE));
    db_query("REPLACE INTO {boxes} (bid, body, info, format) VALUES(%d, '%s', '%s', %d)", (int)$block->delta, $box->body, $box->info, $box->format);
  }
  return 1;
}

function incdep_service_delete_node($uuid) {
  $nid = db_result(db_query("SELECT n.nid FROM {node} n INNER JOIN {node_uuid} u ON n.nid = u.nid WHERE u.uuid = '%s'", $uuid));
  if ($nid) {
    node_delete($nid);
  }
  $result = db_result(db_query("SELECT nid FROM {node} WHERE nid = %d", $nid));
  if ($result) {
    return services_error('The node could not be deleted.', 400);
  }
  return 1;
}

/**
 * Invoke the cache clearing hook in all enabled modules
 */
function incdep_service_invoke_post_deploy_tasks() {
  module_invoke_all('deploy_done_remote');
  return 1;
}


/**
 * access callback function for the block service
 */
function incdep_service_save_block_access() {
  if (user_access('administer blocks')) {
    return TRUE;
  }
}