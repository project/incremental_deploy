<?php
// $Id $

/**
 * Saves nodewords information
 */
function incdep_services_extra_save_nodewords($nw) {
  foreach ($nw['tables'] as $table => $details) {
    // If the nodewords deployment consists of another table besides the nodewords
    // table itself, we need to process it first, and then use its primary key as
    // the id field in the nw table.
    if ($table != 'nodewords') {
      // find out what the table's primary key is as this is used as the id
      // field in the nodewords table
      $key = $details['key'];
      // see if we have this row in the table already, going by the 'lookup' field
      if (isset($details['lookup'])) {
        $lookup = $details['lookup'];
        $id = db_result(db_query("SELECT {$key} FROM {$table} WHERE {$lookup} = '%s'", $details[$lookup]));
      }
      if (!$id) {
        // unset this as we'll be getting a new one on insert
        unset($details[$key]);
        $details = (object) $details;
        drupal_write_record($table, $details);
        $id = $details->{$key};
      }
    }
  }
  $nodewords = (object) $nw['tables']['nodewords'];
  if (isset($id)) {
    $nodewords->id = $id;
  }
  $mtid = db_result(db_query("SELECT mtid FROM {nodewords} WHERE type = %d AND id = %d AND name = '%s'", $nodewords->type, $nodewords->id, $nodewords->name));
  if ($mtid) {
    db_query("UPDATE {nodewords} SET content = '%s' WHERE mtid = %d", $nodewords->content, $mtid);
    $saved = TRUE;
  }
  else {
    $saved = drupal_write_record('nodewords', $nodewords);
  }
  return $saved ? $saved : services_error(t('An error occurred trying to write %id', array('%id' => $nodewords->id)), 400);
}

/**
 * Saves a subqueue
 * This would ideally be handled by nodequeue_service module - see the patch at
 * http://drupal.org/node/1045678 but in the meantime we'll keep it here.
 */
function incdep_services_extra_save_subqueue($subqueue) {
  $subqueue = (object) $subqueue;
  $is_basic = FALSE; // Whether the node queue is just a standard queue with a single subqueue
  if (isset($subqueue->queue_name)) {
    // We must get the qid by the machine name of the queue
    // We also need to know the owner of the queue to determine what the refernce value will refer to.
    $result = db_query("SELECT qid, owner FROM {nodequeue_queue} WHERE name = '%s'", $subqueue->queue_name);
    if ($row = db_fetch_array($result)) {
      $qid = $row['qid'];
      $is_basic = $row['owner'] == 'nodequeue';
    }
    else {
      return services_error(t('Specified node queue does not exist.'));
    }
  }
  elseif (isset($subqueue->qid)) {
    $qid = $subqueue->qid;
    $owner = db_result(db_query("SELECT owner from {nodequeue_queue} WHERE qid = %d", $qid));
    if (!$owner) {
      return services_error(t('Specified node queue does not exist.'));
    }
    $is_basic = $owner == 'nodequeue';
  }
  else {
    return services_error(t('No queue was specified.'));
  }

  // If this is a basic nodequeue, then we should use the local qid as our reference
  // when looking for the sqid.
  $ref = $is_basic ? $qid : $subqueue->reference;
  
  // Get the local sqid for this subqueue.
  $sqid = db_result(db_query_range("SELECT sqid FROM {nodequeue_subqueue} WHERE qid = %d AND reference = '%s'", $qid, $ref, 0, 1));
  if (!$sqid) {
    return services_error(t('Specified subqueue does not exist.'));
  }
  $saved = nodequeue_save_subqueue_order($subqueue->nodes, $qid, $sqid);
  if ($saved[0] != NODEQUEUE_OK) {
    return services_error(t('Invalid subqueue order, could not be saved.'));
  }
  
  // This won't be necessary if/when http://drupal.org/node/992326 gets in.
  // Basically, the changed subqueue order should get added to the plan, if this
  // current environment is also running nodequeue_deploy module.
  if (module_exists('nodequeue_deploy')) {
    nodequeue_deploy_add_sq_incdeploy($sqid);
  }
  return $sqid;
}

/**
 * Flags or unflags a node, comment or user
 */
function incdep_services_extra_set_flagging($flagging) {
  $flagging = (object) $flagging;
  $flag = flag_get_flag($flagging->flag_name);
  if (!$flag) {
    return services_error(t('Specified flag does not exist.'));
  }
  if ($flag->flag($flagging->op, $flagging->content_id, NULL, TRUE)) {
    watchdog('incdep_services_extra_flag', 'Just flagged the content_id: @cid', array('@cid' => $flagging->content_id));
  }
  else {
    return services_error(t('Unable to flag content.'));
  }

  return 1;
}


/**
 * access callback function for the nodewords service
 */
function incdep_services_extra_save_nodewords_access() {
  if (user_access('administer meta tags')) {
    return TRUE;
  }
}

/**
 * access callback function for the nodequeue.save service
 */
function incdep_services_extra_save_nodequeue_access() {
  if (user_access('manipulate all queues')) {
    return TRUE;
  }
}