<?php

/**
 * Administrative form for Incremental Deploy settings.
 */
function incremental_deploy_settings_form() { 
  $form['incremental_deploy_items_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Items per page'),
    '#description' => t('The number of items to show per page when viewing the latest changes to be deployed'),
    '#default_value' => variable_get('incremental_deploy_items_per_page', 20),
  );
  $form['incremental_deploy_ignore'] = array(
    '#type' => 'textarea',
    '#title' => t('Variables to ignore'),
    '#description' => t('Add the prefixes or full names of variables that should never be added to incremental deployment plans. They will all be treated as prefixes so if you put in e.g. "my_var", this will match both the "my_var" and "my_var_other" variables'),
    '#default_value' => variable_get('incremental_deploy_ignore', "deploy_\r\nincremental_deploy_\r\nincdeploy_\r\nmenu_\r\nform_build_id_\r\njavascript_parsed\r\ncss_js_querystring"),
    '#size' => 100,
    '#rows' => 6,
  );  
  return system_settings_form($form);
}

/**
 * The menu callback for the "main" incremental deploy page.
 * This lists all items in the active plan and provides the option of
 * selecting items and moving them either to the postponed or to the
 * blocked plan.
 */
function incremental_deploy_latest() {
  // Check that there isn't already a deployment in progress
  if (incremental_deploy_check_status() != INCREMENTAL_DEPLOY_SESSION_IN_PROGRESS) {
    // Make sure all triggerable items get added to the active or postponed plan
    incremental_deploy_update_active_plan();
    // Get the active plan. We pass TRUE here to make sure we get the actual
    // active plan and are not diverted to the postponed plan.
    $pid = incremental_deploy_get_active_pid(TRUE);
    module_load_include('inc', 'deploy', 'deploy.plans.admin');
    $items = incremental_deploy_get_plan_items($pid, TRUE, TRUE, 'iid', 'DESC');
    if (empty($items)) {
      $output = t('There are no new items to be deployed.');
    }
    else {
      if (!variable_get('incdeploy_filtered_deploy', FALSE)) {
        $ts = variable_get('incdeploy_last_ts', '');
        if ($ts == '') {
          $output = t('Items that have changed on the site');
        }
        else {
          $output = t('Items that have changed since last deployment at @time on @date', array('@time' => date('h:m:s', $ts), '@date' => date('j M Y', $ts)));
        }
      }
      else {
        $output = t('Items currently filtered for deployment.');
        $output .= t('Click !here to continue with this deployment as is.', array('!here' => l('here', 'admin/build/deploy/push/'. $pid)));
      }

      $output .= drupal_get_form('incremental_deploy_list_form', 'active', $pid, $items);
      $output .= theme('pager');
      $output .= '<div id="plan-current-items"></div>';
    }
  }
  else {
    $output = t('There is currently a deployment in progress. Please check back later.');
  }

  return $output;
}

/**
 * The menu callback for the page that lists all items in the 'postponed' plan.
 */
function incremental_deploy_postponed() {
  $pid = incremental_deploy_get_postponed_pid();
  module_load_include('inc', 'deploy', 'deploy.plans.admin');
  $items = incremental_deploy_get_plan_items($pid, TRUE, TRUE);
  if (empty($items)) {
    $output = t('There are no postponed items.');
  }
  else {
    $output = drupal_get_form('incremental_deploy_list_form', 'postponed', $pid, $items);
    $output .= theme('pager');
  }
  return $output;
}

/**
 * The menu callback for the page that lists all items in the 'blocked' plan.
 */
function incremental_deploy_blocked() {
  $pid = incremental_deploy_get_blocked_pid();
  module_load_include('inc', 'deploy', 'deploy.plans.admin');
  $items = incremental_deploy_get_plan_items($pid, TRUE, TRUE);
  if (empty($items)) {
    $output = t('There are no blocked items.');
  }
  else {
    $output = drupal_get_form('incremental_deploy_list_form', 'blocked', $pid, $items);
    $output .= theme('pager');
  }
  return $output;
}

/**
 * Display a list of all items in an incremental plan.
 *
 * @param $form_state
 *   FAPI form state
 * @param $pid
 *   Unique identifier for the plan we're viewing
 * @return
 *   FAPI array
 * @ingroup forms
 * @see incremental_deploy_list_form_submit()
 */
function incremental_deploy_list_form($form_state, $type = 'active', $pid = 0, $items = array()) {

  // Bail if there are no plan items
  if (is_null($pid) || empty($items)) {
    return NULL;
  }
  $form = array(
    '#theme' => 'incremental_deploy_list_form',
  );
  
  $item_count = count($items);
  $last_iid = $items[$item_count-1]['iid'];
  // Hold all of the child items of this plan in an array
  $child_items = array();
  $result = db_query("SELECT * FROM {deploy_plan_items} WHERE pid = %d AND parent >= %d", $pid, $last_iid);
  while ($row = db_fetch_array($result)) {
    $child_items[$row['parent']][] = $row;
  }

  // username cache so we don't have to user_load constantly
  $user_cache = array();

  // Build form tree
  $form['#tree'] = TRUE;
  $plan_items = array();
  foreach ($items as $i => $item) {
    if (!array_key_exists($item['uid'], $user_cache)) {
      $account = user_load(array('uid' => $item['uid']));
      $user_cache[$item['uid']] = $account->name;
    }
    // Allow other modules to alter how we display the item.
    drupal_alter('incdeploy_listing', $item);
    $added_by = $user_cache[$item['uid']] ." on ". format_date($item['ts'], 'small');
    $form[$i]['module'] = array('#type' => 'item', '#value' => $item['module']);
    $form[$i]['description'] = array(
      '#type' => 'item',
      '#value' => $item['description']
    );
    $form[$i]['added_by'] = array('#type' => 'item', '#value' => $added_by);
    $form[$i]['iid'] = array('#type' => 'hidden', '#value' => $item['iid']);
  
    $plan_items[$item['iid']] = '';
    if (is_array($child_items[$item['iid']])) {
      $form[$i]['children'] = array();
      foreach ($child_items[$item['iid']] as $j => $child) {
        drupal_alter('incdeploy_listing', $child);
        $child_added = $user_cache[$child['uid']] ." on ". format_date($child['ts'], 'small');
        $form[$i]['children'][$j] = array(
          'module' => array('#type' => 'item', '#value' => $child['module']),
          'description' => array(
            '#type' => 'item',
            '#value' => $child['description']
          ),
          'added_by' => array('#type' => 'item', '#value' => $child_added),
        );
      }
    }
  }

  $form['plan_items'] = array('#type' => 'checkboxes', '#options' => $plan_items);
  $form['pid'] = array('#type' => 'hidden', '#value' => $pid);
  switch ($type) {
      case 'active':
        $form['deploy'] = array('#type' => 'submit', '#value' => t('Deploy Selected'));
        $form['delete'] = array('#type' => 'submit', '#value' => t('Delete Selected'));
        $form['block'] = array('#type' => 'submit', '#value' => t('Block Selected'));
        break;
      case 'blocked':
      case 'postponed':
        $form['activate'] = array('#type' => 'submit', '#value' => t('Move to active plan'));
  }
  $form['#redirect'] = $_GET['q'];
  return $form;
}


/**
 * Validation callback for incremental_deploy_list_form().
 */
function incremental_deploy_list_form_validate($form, &$form_state) {
  // If the 'Deploy selected' button was clicked, make sure at least one item
  // was selected.
  switch ($form_state['clicked_button']['#id']) {
    case 'edit-deploy':
      $items = array_filter($form_state['values']['plan_items']);
      if (empty($items)) {
        form_set_error('plan_items', t('You must select at least one item for deployment'));
      }
      break;
  }
}

/**
 * Submit callback for incremental_deploy_list_form().
 */
function incremental_deploy_list_form_submit($form, &$form_state) {
  if (incremental_deploy_check_status() != INCREMENTAL_DEPLOY_SESSION_IN_PROGRESS) {
    $items = array_filter($form_state['values']['plan_items']);
    // check which submit button was clicked and move items accordingly
    switch ($form_state['clicked_button']['#id']) {
      case 'edit-deploy':
        
        // Setting this variable to true will ensure that other items will get
        // diverted to the postponed plan until this one has been deployed.
        variable_set('incdeploy_filtered_deploy', TRUE);

        // This variable holds the timestamp for the time against which we compare
        // triggerable updates (i.e. variables and blocks).
        variable_set('incdeploy_last_triggered_ts', time());
        
        // We postpone all the items that were NOT checked - so anything currently
        // in the active plan, other than what was checked, must be moved.
        $result = db_query("SELECT iid FROM {deploy_plan_items} WHERE pid = %d AND parent = 0 AND iid NOT IN (" . implode(", ", $items) .  ")", $form_state['values']['pid']);
        $new_items = array();
        while ($row = db_fetch_array($result)) {
          $new_items[$row['iid']] = $row['iid'];
        }
        $items = $new_items;
        $to = 'postponed';
        $goto = 'admin/build/deploy/push/'. $form_state['values']['pid'];
        break;
      case 'edit-block':
        $to = 'blocked';
        break;
      case 'edit-activate':
        $to = 'active';
        break;
      case 'edit-delete':
        drupal_goto('admin/build/incremental_deploy/'. implode(',', $items) .'/delete');
    }
    incremental_deploy_move_items($to, $items);
    if (isset($goto)) {
      drupal_goto($goto);
    }
  }
}

function incremental_deploy_delete(&$form_state, $items) {
  $items_arr = explode(',', $items);
  $form = array();
  $form['to_delete'] = array(
    '#type' => 'value',
    '#value' => $items,
  );
  return confirm_form($form,
    t('Remove @count items from the plan?', array('@count' => count($items_arr))),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/build/incremental_deploy',
    t('This action cannot be undone.'),
    t('Remove'), t('Cancel')
  );
}

/**
 * Submit function for incremental_deploy_delete
 * Deletes the specified items from the current plan.
 */
function incremental_deploy_delete_submit($formid, &$form_state) {
  if ($form_state['values']['confirm']) {
    $items = explode(',', $form_state['values']['to_delete']);
    foreach ($items as $item) {
      db_query("DELETE FROM {deploy_plan_items} WHERE iid = %d", $item);
    }
    drupal_set_message(t('The items have been deleted from the plan.'));
  }
  $form_state['redirect'] = 'admin/build/incremental_deploy';
}

/**
 * Menu callback for deployables page.
 */
function incremental_deploy_check_deployables() {
  $items = array();
  $modules = module_implements('incdeploy_info');
  foreach ($modules as $module) {
    $items = array_merge($items, module_invoke($module, 'incdeploy_info'));
  }
  foreach (module_implements('deploy') as $module) {
    if (!in_array($module, $modules)) {
      $deployables[] = $module;
    }
  }
  return theme('incdeploy_deployables', $items, $deployables);
}

function theme_incdeploy_deployables($items = array(), $deployables = array()) {
  $headers = array(
    t('Entity type'),
    t('Actions'),
  );
  $rows = array();
  foreach ($items as $entity => $actions) {
    foreach ($actions as $i => $action) {
      // Build the table row.
      $row = array(
        'data' => array(
          array('data' => $i == 0 ? $entity : ''),
          array('data' => $action),
        ),
      );
      $rows[] = $row;
    }
  }

  $output = theme('table', $headers, $rows);
  $output .= t('The following entities have basic deployability, i.e. add and edit');
  $headers = array(
    t('Entity type'),
  );
  $rows = array();
  foreach ($deployables as $deployable) {
    // Build the table row.
    $row = array(
      'data' => array(
        array('data' => $deployable),
      ),
    );
    $rows[] = $row;
  }
  $output .= theme('table', $headers, $rows);
  return $output;
}


function theme_incremental_deploy_list_form($form) {
  drupal_add_js(drupal_get_path('module', 'incremental_deploy') .'/js/incremental_deploy.js');
  $header = array(theme('table_select_header_cell'), t('Module'), t('Description'), t('Added by'));
  $rows = array();

  foreach (element_children($form) as $i) {
    $item =& $form[$i];
    if (is_array($item['module'])) {
      $is_parent = !empty($item['children']);
      $rows[] = array(
        'data' => array(
          drupal_render($form['plan_items'][$item['iid']['#value']]),
          drupal_render($item['module']),
          drupal_render($item['description']),
          drupal_render($item['added_by']),
        ),
        'class' => $is_parent ? 'has-children' : '',
        'id' => $is_parent ? 'parent-'. $i : '',
      );
      if (is_array($item['children'])) {
        foreach (element_children($item['children']) as $j) {
          $child =& $form[$i]['children'][$j];
          $rows[] = array(
            'data' => array(
              '',
              drupal_render($child['module']),
              drupal_render($child['description']),
              drupal_render($child['added_by']),
            ),
            'class' => 'deploy-item-child child-of-'. $i,
          );
        }
      }
    }
  }
  
  $output = theme('table', $header, $rows, array('id' => 'plans'));
  $output .= drupal_render($form);

  return $output;
}