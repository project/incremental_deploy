Drupal.behaviors.incdep = function (context) {
  $('tr.deploy-item-child', context).hide();
  $('#plans tr.has-children', context).each(function() {
    var id = $(this).attr('id').split('-')[1];
    var $link = $('<a href="#" class="show-children"></a>').text('+').addClass('collapsed').prependTo($($(this).find('div.form-item').get(1))).click(function() {
      $(this).toggleClass('collapsed');
      $('tr.deploy-item-child', context).filter('.child-of-' + id).toggle();
      return false;
    });
  });
}
